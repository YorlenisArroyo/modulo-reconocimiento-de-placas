def placas():
    import re
    print('\t***ATT***\n')
    placa= input("Digite la numero de Placa: ")


    if re.match('(^T)([0-9]{5}$)', placa):
        print("Taxi")
    elif re.match('(^M)(B)([0-9]{4}$)', placa):
        print("Metro Bus")
    elif re.match('(^B)([0-9]{5}$)', placa):
        print("Bus")
    elif re.match('(^M)([0-9]{4,5}$)', placa):
        print("Moto")
    elif re.match('(^D)([0-9]{5}$)', placa):
        print("Placas de Demostracion o prueba")
    elif re.match('(^C)(D)([0-9]{4}$)', placa):
        print("Cuerpo Diplomatico")
    elif re.match('(^C)(C)([0-9]{4}$)', placa):
        print("Cuerpo Consular")
    elif re.match('(^J)([0-9]{5}$)', placa):
        print("Juez o Fiscal")
    elif re.match('(^P)(R)([0-9]{4}$)', placa):
        print("Periodista")
    elif re.match('[A-Z-0-9]{6}$', placa):
        print("Particular")

    else:
     print("Sin Concidencia")
